import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-superlogin',
  templateUrl: './superlogin.component.html',
  styleUrls: ['./superlogin.component.scss']
})
export class SuperloginComponent implements OnInit {
  loginForm: FormGroup;
  error_msg: any;
  isLoading: boolean;

  constructor(private api: ApiService, private router: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
      this.loginForm = this.fb.group({
        username: ['', [Validators.required]],
        password: ['', [Validators.required]]
      });
      
  }

  logIn(){
    if (this.loginForm.valid) {
      this.onSubmitlogin(this.loginForm.value);
     
    }

   
  }
  onSubmitlogin(data){
   
    let response = this.api.superLogin(data);
    response.subscribe((response: any)=>{
     
      if (response) {
        this.api.setLoadingStatus(false);
          window.sessionStorage.setItem("amasunzu", JSON.stringify(response));
          this.router.navigate(["/super-dashboard"]);
        }
        else {
         
        this.router.navigate(["/super-login"]);
        }
        
      
    }, error =>{
     
      if (error.status == 403) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Payment required to use the system"
      }
      else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.error_msg = "Wrong Username or Password"

      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.error_msg = "No internet connection"

      }
      
      
    })

  }

}


