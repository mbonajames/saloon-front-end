import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-visitreport',
  templateUrl: './visitreport.component.html',
  styleUrls: ['./visitreport.component.scss']
})
export class VisitreportComponent implements OnInit {
  periodForm: FormGroup;
  error_msg: any;
  report: any;
  total_price:any;
  total_clients: any;
  error: any;
  totals: any;
  expenses: any;
  total_expenses: any;
  isLoading: any;
  report_message: any;
  start: any;
  end: any;

  constructor(private api: ApiService, private fb: FormBuilder) { }
  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    const profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const saloon_id = profile.id
    this.api.setLoadingStatus(false);
    this.periodForm = this.fb.group({
      saloon: [saloon_id, [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]]
    });

  }
  
getReport(data){
  this.error = null;
  this.start = data.startDate
  this.end = data.endDate
    this.report = null;
    let response = this.api.getPeriodicvisit(data);
   
    response.subscribe((response: any)=> {
      this.api.setLoadingStatus(false);
          this.report = response;
          console.log(this.report)
          this.api.setLoadingStatus(false);
         
        
      },
      error => {
        this.error = error;
        this.api.setLoadingStatus(false);
        if (error.status == 400) {
         
          this.report_message = "No Records"
        }
        else if (error.status == 500) {
         
          this.report_message = "Enter start and end dates" 
        }
      }
    );
  }
  print(){
    window.print()
  }

}
