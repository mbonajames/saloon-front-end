import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-supersidenav',
  templateUrl: './supersidenav.component.html',
  styleUrls: ['./supersidenav.component.scss']
})
export class SupersidenavComponent implements OnInit {
  saloon: any;
  isAuthenticated: boolean;
 
   constructor(private router: Router, private api: ApiService) { }
 
   ngOnInit(): void {
     const profile= JSON.parse(window.sessionStorage.getItem("amasunzu"));
    
     
     
     if (!profile) {
      
         this.router.navigate(["/super-login"]);
       
     } 
    
   }
 logOut(){
   window.sessionStorage.clear();
   this.router.navigate(["/super-login"]);
 }
 }
 