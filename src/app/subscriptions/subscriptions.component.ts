import { Component, OnInit } from '@angular/core';
import {formatDate} from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.scss']
})
export class SubscriptionsComponent implements OnInit {
  error:any;
  report:any;
  client: any;
  isAdded: Boolean;
  message: any;
  expenseIsAdded: any;
  total_expenses: any;
  expense_message: any;
  date = new Date();
  total_price: any;
  expenses: any;
  total_clients: any;
  addClinetForm: FormGroup;
  addmessage: any;
  addSubForm: FormGroup;
  profile: any;
  isLoading: boolean;
  employees: any;

  constructor(private api: ApiService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    this.profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const saloon_id = this.profile.id
    const day = formatDate(this.date, 'yyyy-MM-dd', 'en-US');
    console.log(saloon_id)

    this.addSubForm = this.fb.group({
      saloon: [saloon_id, [Validators.required]],
      name: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      district: ['', [Validators.required]],
      sector: ['', [Validators.required]],
      cell: ['', [Validators.required]],
      village: ['', [Validators.required]],
      service: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      eligibility: ['eligible', [Validators.required]],
      times: ['', [Validators.required]],
      due_date: ['', [Validators.required]],
     
    });

    this.addClinetForm = this.fb.group({
      saloon: [saloon_id, [Validators.required]],
      name: [this.client ? this.client.name :'', [Validators.required]],
      phone: [this.client ? this.client.phone :'', [Validators.required]],
      district: [this.client ? this.client.district :'', [Validators.required]],
      sector: [this.client ? this.client.sector :'', [Validators.required]],
      cell: [this.client ? this.client.cell :'', [Validators.required]],
      village: [this.client ? this.client.village :'', [Validators.required]],
      service: [this.client ? this.client.service :'', [Validators.required]],
      price: [0, [Validators.required]],
      barber: ['', [Validators.required]],
    });
    this.getSubscribers(saloon_id, day);
    this.getEmployees(saloon_id);
  }
  addSub(data){
    console.log(data);
    if (this.addSubForm.valid) {
   
     let response = this.api.addSubscriber(data);
     response.subscribe((response: any)=>{
      this.api.setLoadingStatus(false);
      this.isAdded = true;
      this.message = "Added"
      this.ngOnInit();
 
     }, error =>{
      
       if (error.status == 403) {
         this.api.setLoadingStatus(false);
         this.isAdded = true;
         this.message = "Not Added"
       }
       else if (error.status == 400) {
         this.api.setLoadingStatus(false);
         this.isAdded = true;
         this.message = "Not Added"
       }
       else if (error.status == 0) {
         this.api.setLoadingStatus(false);
         this.isAdded = true;
         this.message = "Not Added, No internet Available"
       }
       
       
     })
   }
 
   }
   getSubscribers(saloon, day) {
    this.error = null;
    this.report = null;
    let response = this.api.getSubscribers(saloon,day);
    response.subscribe((response: any)=> {
        
          this.report = response;
          console.log(this.report)
          this.api.setLoadingStatus(false);
          
        
      },
      error => {
        this.error = error;
        this.api.setLoadingStatus(false);
      }
    );
  }

  addClient(data){
    if (this.addClinetForm.valid) {
   
     let response = this.api.addClient(data);
     response.subscribe((response: any)=>{
      this.api.setLoadingStatus(false);
      this.isAdded = true;
      this.addmessage = "Added"
      this.editTimes(this.client.id)
      this.ngOnInit();
 
     }, error =>{
      
       if (error.status == 403) {
         this.api.setLoadingStatus(false);
         this.isAdded = true;
         this.addmessage = "Not Added"
       }
       else if (error.status == 400) {
         this.api.setLoadingStatus(false);
         this.isAdded = true;
         this.addmessage = "Not Added"
       }
       else if (error.status == 0) {
         this.api.setLoadingStatus(false);
         this.isAdded = true;
         this.addmessage = "Not Added, No internet Available"
       }
       
       
     })
   }
 
   }
   add(data){
    this.client = data
    this.ngOnInit()
   }
   
   getEmployees(saloon){
    let response = this.api.getEmployees(saloon);
     response.subscribe((response: any)=>{
      this.api.setLoadingStatus(false);
     this.employees = response
     
 
     }, error =>{
      
       if (error.status == 403) {
        this.api.setLoadingStatus(false);
        
       }
       else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        
       }
       else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        
       }
       
       
     })
   }
   editTimes(client_id){
    let response = this.api.editTimes(client_id);
    response.subscribe((response: any)=>{
     this.api.setLoadingStatus(false);
    

    }, error =>{
     
      if (error.status == 403) {
       this.api.setLoadingStatus(false);
       
      }
      else if (error.status == 400) {
       this.api.setLoadingStatus(false);
       
      }
      else if (error.status == 0) {
       this.api.setLoadingStatus(false);
       
      }
      
      
    })

   }
   reload(){
     this.ngOnInit()
   }
}
