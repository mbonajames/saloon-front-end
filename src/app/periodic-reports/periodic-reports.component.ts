import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-periodic-reports',
  templateUrl: './periodic-reports.component.html',
  styleUrls: ['./periodic-reports.component.scss']
})
export class PeriodicReportsComponent implements OnInit {
  periodForm: FormGroup;
  error_msg: any;
  report: any;
  total_price:any;
  total_clients: any;
  error: any;
  totals: any;
  expenses: any;
  total_expenses: any;
  isLoading: any;
  report_message: any;
  start: any;
  end: any;

  constructor(private api: ApiService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    this.api.setLoadingStatus(false);
    const profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const saloon_id = profile.id
    
      this.periodForm = this.fb.group({
        saloon: [saloon_id, [Validators.required]],
        startDate: ['', [Validators.required]],
        endDate: ['', [Validators.required]]
      });

}

getReport(data){
  this.error = null;
  this.start= data.startDate
  this.end = data.endDate
    this.report = null;
    let response = this.api.getPeriodicReport(data);
    this.getServiceTotals(data)
    this.getPeriodicExpenses(data)
    response.subscribe((response: any)=> {
        
          this.report = response;
          this.report_message = null
          console.log(this.report)
          this.ngOnInit()
          this.api.setLoadingStatus(false);
          let price = 0
         
          for (let i = 0; i < this.report.data.length; i++) {
            let data = this.report.data[i];
            price = price + parseInt(data.income, 10)
            
           
            // this.total_clients = clients
          }
          this.total_price = price;
        
      },
      error => {
        this.error = error;
        this.api.setLoadingStatus(false);
        if (error.status == 400) {
         
          this.report_message = "No Records"
        }
        else if (error.status == 500) {
         
          this.report_message = "Enter start and end dates" 
        }
      }
    );
  }
  getServiceTotals(data) {
    this.error = null;
    this.totals = null;
    let response = this.api.getPeriodicServiceTotals(data);
    response.subscribe((response: any)=> {
        
          this.totals = response;
          console.log(this.totals)
          this.api.setLoadingStatus(false);
         
          let clients = 0
          for (let i = 0; i < this.totals.length; i++) {
           
            clients = clients + parseInt(this.totals[i].clients)
          }
          this.total_clients = clients
         

      },
      error => {
        this.error = error;
        this.api.setLoadingStatus(false);
        if (error.status == 400) {
         
          this.report_message = "No Records"
        }
        else if (error.status == 500) {
         
          this.report_message = "Enter start and end dates" 
        }
      }
    );
  }
  getPeriodicExpenses(data) {
    this.error = null;
    this.totals = null;
    let response = this.api.getPeriodicExpenses(data);
    response.subscribe((response: any)=> {
        
          this.expenses = response;
          console.log(this.expenses)
          this.api.setLoadingStatus(false);
          this.error_msg = null
          let price = 0
          for (let i = 0; i < this.expenses.length; i++) {
            price = price + parseInt(this.expenses[i].price) 
           
          }
          
          this.total_expenses = price

      },
      error => {
        this.error = error;
        if (error.status == 400) {
          this.error_msg = "No Expenses"
        }
      }
    );
  }

}
