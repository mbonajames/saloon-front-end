import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
profile: any;
error: any;
services: any;

services_data: any;
profileForm: FormGroup;
serviceForm: FormGroup;
settingsForm: FormGroup;
profile_message: any;
service_message: any;
isLoading: boolean;

  constructor(private api: ApiService, private fb: FormBuilder, private router: Router) { }
  
  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    this.profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const saloon_id = this.profile.id
   
    this.getServices(saloon_id);
    

    this.profileForm = this.fb.group({
      saloon: [saloon_id, [Validators.required]],
      name: [this.profile ? this.profile.name :'', [Validators.required]],
      email: [this.profile ? this.profile.email :'', [Validators.required]],
      phone: [this.profile ? this.profile.phone :'', [Validators.required]],
      permission: [this.profile.permission, [Validators.required]],
      
    });

    this.serviceForm = this.fb.group({
      saloon: [saloon_id, [Validators.required]],
      service: ['', [Validators.required]],
      price: ['', [Validators.required]],
     
      
    });

    
  }

  getServices(data){
   
    let response = this.api.getServices(data);
    response.subscribe((response: any)=> {
      this.api.setLoadingStatus(false);
          this.services = response;
          console.log(this.services)
          this.api.setLoadingStatus(false);
         
      },
      error => {
        this.error = error;
        this.api.setLoadingStatus(false);
      }
    );
  }
  
 
editProfile(data){
  if (this.profileForm.valid) {
   
    let response = this.api.editProfile(this.profile.id,data);
    response.subscribe((response: any)=>{
    window.sessionStorage.setItem("profile", JSON.stringify(response));
    this.api.setLoadingStatus(false);
     this.profile_message = "Updated"
     this.ngOnInit();

    }, error =>{
      this.api.setLoadingStatus(false);
      if (error.status == 403) {
       
        this.profile_message = "Not Updated"
        this.router.navigate(["/login"]);
      }
      else if (error.status == 400) {
       
        this.profile_message = "Not Updated Wrong inputs"
      }
      else if (error.status == 0) {
       
        this.profile_message = "Not Updated, No internet Available"
      }
      
      
    })
  }

}

addService(data){
  if (this.serviceForm.valid) {
   
    let response = this.api.addService(data);
    response.subscribe((response: any)=>{
      this.api.setLoadingStatus(false);
     this.service_message = "Added"
     this.ngOnInit();

    }, error =>{
      this.api.setLoadingStatus(false);
      if (error.status == 403) {
       
        this.service_message = "Not Added"
       
      }
      else if (error.status == 400) {
       
        this.service_message = "Not Added Wrong inputs"
      }
      else if (error.status == 0) {
       
        this.service_message = "Not Added, No internet Available"
      }
      
      
    })
  }
}

// openEditor(data){
// this.services_data = data
// this.ngOnInit()
// }

remove(){
  this.profile_message = null
  this.service_message = null
 
}
}
