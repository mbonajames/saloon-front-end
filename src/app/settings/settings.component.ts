import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  user: any;
  settings_message: any;
  profile: any;
  settingsForm: FormGroup;
  error: any;
  edit_password: boolean;
  isLoading: boolean;
  constructor(private api: ApiService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    this.edit_password = false
    this.profile= JSON.parse(window.sessionStorage.getItem("profile"));
    this.api.setLoadingStatus(false);
    console.log(this.profile.user)
    this.settingsForm = this.fb.group({
      saloon: [this.profile.id, [Validators.required]],
      username: [this.profile.user ? this.profile.user.username :'', [Validators.required]],
      email: [this.profile.user ? this.profile.user.email :'', [Validators.required]],
      password: [this.profile.user ? this.profile.user.password :'', [Validators.required]],
      current_password: ['', [Validators.required]],
      category: [this.profile.user.category, [Validators.required]],
     
      
    });
  }
  

  editSettings(data){
    if (this.settingsForm.valid) {
      if (this.settingsForm.value.current_password == this.profile.user.password ) {
        let response = this.api.editUser(this.profile.user.id,data);
      response.subscribe((response: any)=>{
        this.api.setLoadingStatus(false);
      window.sessionStorage.clear()
        this.router.navigate(["/login"]);
       this.ngOnInit();
  
      }, error =>{
        this.api.setLoadingStatus(false);
       
        if (error.status == 400) {
         
          this.settings_message = "Not Updated Wrong inputs"
        }
        else if (error.status == 0) {
         
          this.settings_message = "Not Updated, No internet Available"
        }
        
        
      })
    }
    else{
      this.settings_message = "Current Password did not match"
    }
      }
     
      
  
  }
  editPassword(){
    this.edit_password = true
  }

}
