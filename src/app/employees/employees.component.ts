import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {
  error:any;
  report:any;
  employees: any;
  isAdded: Boolean;
  getmsg: any;
  edit: boolean;
  message: any;
  date = new Date();
  total_price: any;
  total_clients: any;
  addEmployeeForm: FormGroup;
  editEmployeeForm: FormGroup;
  employeeData: any;
  delete_id: any;
  delete_name: any;
  isLoading: boolean;
  constructor( private api: ApiService, private fb: FormBuilder) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    const profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const saloon_id = profile.id

    this.getEmployees(saloon_id)

    this.addEmployeeForm = this.fb.group({
      saloon: [saloon_id, [Validators.required]],
      name: [this.employeeData ? this.employeeData.name :'', [Validators.required]],
      phone: [this.employeeData ? this.employeeData.phone :'', [Validators.required]],
      email: [this.employeeData ? this.employeeData.email :'', [Validators.required]],
      nid: [this.employeeData ? this.employeeData.nid :'', [Validators.required]],
    });
    
  }
  addEmployee(data){
    if (this.addEmployeeForm.valid) {
   
     let response = this.api.addEmployee(data);
     response.subscribe((response: any)=>{
      this.api.setLoadingStatus(false);
      this.isAdded = true;
      this.message = "Added"
      this.ngOnInit();
 
     }, error =>{
      
       if (error.status == 403) {
        this.api.setLoadingStatus(false);
         this.isAdded = true;
         this.message = "Not Added"
       }
       else if (error.status == 400) {
        this.api.setLoadingStatus(false);
         this.isAdded = true;
         this.message = "Not Added Wrong inputs"
       }
       else if (error.status == 0) {
        this.api.setLoadingStatus(false);
         this.isAdded = true;
         this.message = "Not Added, No internet Available"
       }
       
       
     })
   }
 
   }

   getEmployees(saloon){
    let response = this.api.getEmployees(saloon);
     response.subscribe((response: any)=>{
      this.api.setLoadingStatus(false);
     this.employees = response
     
 
     }, error =>{
      
       if (error.status == 403) {
        this.api.setLoadingStatus(false);
         this.getmsg = "No Employees"
       }
       else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.getmsg = "No Employees"
       }
       else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.getmsg = "No Internet Avialable"
       }
       
       
     })
   }

   openEditor(employee){
    this.employeeData = employee
    this.edit = true
    this.message = null
    this.ngOnInit()
    
   }
   editEmployee(){
    if (this.addEmployeeForm.valid) {

        this.api.updateEmployee(this.employeeData.id, this.addEmployeeForm.value).subscribe((response) => {
          this.api.setLoadingStatus(false);
          this.message = "updated";
          this.employeeData = null
          this.edit = false
          this.ngOnInit()

        },  error =>{
          this.api.setLoadingStatus(false);
          if (error.status == 400) {
            this.message = "Not updated, Wrong Inputs";
          }
          else if (error.status == 0) {
            this.message = "Not updated, No internet Available";
          }
          
        })
      }

   }
   openDelete(id,name){
    this.delete_id = id
    this.delete_name = name
   }
   deleteEmployee(id){
    
    if (id) {

      this.api.deleteEmployee(id).subscribe((response) => {
        this.api.setLoadingStatus(false);
        this.delete_id = null
        this.delete_name = null
        this.message = "Deleted"
        this.ngOnInit()

      },  error =>{
        this.api.setLoadingStatus(false);
        if (error.status == 400) {
          this.message = "Not deleted, no such employee";
        }
        else if (error.status == 0) {
          this.message = "Not deleted, No internet Available";
        }
        
      })
    }
   }
}
