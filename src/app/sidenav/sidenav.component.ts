import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
 saloon: any;
 isAuthenticated: boolean;

  constructor(private router: Router, private api: ApiService) { }

  ngOnInit(): void {
    const profile= JSON.parse(window.sessionStorage.getItem("profile"));
    this.saloon = profile.name
    
    if (profile) {
      let response = this.api.getPermission(profile.id);
      response.subscribe((response: any)=>{
       
        if (response) {
         
            this.isAuthenticated = true
          }
        
      }, error =>{
       
        if (error.status == 403) {
          window.sessionStorage.clear()
          this.router.navigate(["/"]);
        }
        
      })
      
    }
    if (!profile) {
     
        this.router.navigate(["/login"]);
      
    } 
   
  }
logOut(){
  window.sessionStorage.clear();
  this.router.navigate(["/"]);
}
}
