import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {formatDate} from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  error:any;
  report:any;
  totals: any;
  isAdded: Boolean;
  message: any;
  expenseIsAdded: any;
  total_expenses: any;
  expense_message: any;
  date = new Date();
  total_price: any;
  expenses: any;
  total_clients: any;
  addClinetForm: FormGroup;
  addExpenseForm: FormGroup;
  profile: any;
  isLoading: boolean;
  total_employee_price: any;
  today: any;
  constructor( private api: ApiService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    this.profile= JSON.parse(window.sessionStorage.getItem("profile"));
    const saloon_id = this.profile.id
    const day = formatDate(this.date, 'yyyy-MM-dd', 'en-US');
    this.today = day
    
    //const day = "2020-08-01"
   
    this.getDailyReport(saloon_id,day);
    this.getServiceTotals(saloon_id,day);
    this.getExpenses(saloon_id,day);

    this.addClinetForm = this.fb.group({
      saloon: [saloon_id, [Validators.required]],
      name: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      district: ['', [Validators.required]],
      sector: ['', [Validators.required]],
      cell: ['', [Validators.required]],
      village: ['', [Validators.required]],
      service: ['', [Validators.required]],
      price: ['', [Validators.required]],
      barber: ['', [Validators.required]],
    });

    this.addExpenseForm = this.fb.group({
      saloon: [saloon_id, [Validators.required]],
      expense: ['', [Validators.required]],
      price: ['', [Validators.required]],
      
    });
    
  }

   getDailyReport(saloon, day) {
    this.error = null;
    this.report = null;
    let response = this.api.getDailyReport(saloon,day);
    response.subscribe((response: any)=> {
        
          this.report = response;
          console.log(this.report)
          this.api.setLoadingStatus(false);
           let price = 0
          for (let i = 0; i < this.report.data.length; i++) {
            let data = this.report.data[i];
            price = price + parseInt(data.income, 10)

          }
          this.total_price = price;
        
      },
      error => {
        this.error = error;
        this.api.setLoadingStatus(false);
      }
    );
  }
  getServiceTotals(saloon, day) {
    this.error = null;
    this.totals = null;
    let response = this.api.getServiceTotals(saloon,day);
    response.subscribe((response: any)=> {
        
          this.totals = response;
          console.log(this.totals)
          this.api.setLoadingStatus(false);
         
          let clients = 0
          for (let i = 0; i < this.totals.length; i++) {
            
            clients = clients + parseInt(this.totals[i].clients)
          }
          this.total_clients = clients
         

      },
      error => {
        this.error = error;
        this.api.setLoadingStatus(false);
      }
    );
  }
  getExpenses(saloon, day) {
    this.error = null;
    this.totals = null;
    let response = this.api.getExpenses(saloon,day);
    response.subscribe((response: any)=> {
        
          this.expenses = response;
          console.log(this.expenses)
          this.api.setLoadingStatus(false);
          let price = 0
          for (let i = 0; i < this.expenses.length; i++) {
            price = price + parseInt(this.expenses[i].price) 
           
          }
          
          this.total_expenses = price

      },
      error => {
        this.error = error;
        this.api.setLoadingStatus(false);
      }
    );
  }
  addClient(data){
   if (this.addClinetForm.valid) {
  
    let response = this.api.addClient(data);
    response.subscribe((response: any)=>{
     this.api.setLoadingStatus(false);
     this.isAdded = true;
     this.message = "Added"
     this.ngOnInit();

    }, error =>{
     
      if (error.status == 403) {
        this.api.setLoadingStatus(false);
        this.isAdded = true;
        this.message = "Not Added"
      }
      else if (error.status == 400) {
        this.api.setLoadingStatus(false);
        this.isAdded = true;
        this.message = "Not Added"
      }
      else if (error.status == 0) {
        this.api.setLoadingStatus(false);
        this.isAdded = true;
        this.message = "Not Added, No internet Available"
      }
      
      
    })
  }

  }

  addExpense(data){
    if (this.addExpenseForm.valid) {
      this.expenseIsAdded = false;
     let response = this.api.addExpense(data);
     response.subscribe((response: any)=>{
      this.api.setLoadingStatus(false);
      this.expenseIsAdded = true;
      this.expense_message = "Added"
      this.ngOnInit();
 
     }, error =>{
      
       if (error.status == 403) {
        this.api.setLoadingStatus(false);
         this.expenseIsAdded = true;
         this.expense_message = "Not Added"
       }
       else if (error.status == 400) {
        this.api.setLoadingStatus(false);
         this.expenseIsAdded = true;
         this.expense_message = "Not Added"
       }
       else if (error.status == 0) {
        this.api.setLoadingStatus(false);
         this.expenseIsAdded = true;
         this.expense_message = "Not Added, No internet Available"
       }
       
       
     })
   }
 
   }

}
