import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public error: BehaviorSubject<string> = new BehaviorSubject<string>('');

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  TOKEN: String;

 //BASE_URL = 'http://localhost:8000/api/';
 BASE_URL = 'https://saloonapp-api.herokuapp.com/api/';

  constructor(private http: HttpClient) {
    if (window.sessionStorage.getItem("profile")) {
      this.TOKEN = JSON.parse(window.sessionStorage.getItem("profile")).token;
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Token '+ this.TOKEN
          
        })
      };
      
    }
  }

  // LOADING STATUS FOR INDICATOR
  setLoadingStatus(status: boolean) {
    this.isLoading.next(status);
  }

  // LOGIN

  login(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'login';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  } 

  getPermission(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'permission?id=' + data;
    

    const response = this.http.get(url, this.httpOptions);

    return response;
  } 

  getDailyReport(saloon, day){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'daily-report?saloon=' + saloon +'&day=' + day;
    

    const response = this.http.get(url, this.httpOptions);
    return response;
  }
  getPeriodicReport(data){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'periodic-report';
    

    const response = this.http.post(url, data, this.httpOptions);
    return response;
  }
  getPeriodicServiceTotals(data){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'periodic-service-report';
    

    const response = this.http.post(url, data, this.httpOptions);
    return response;
  }

  getPeriodicExpenses(data){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'periodic-expenses';
    

    const response = this.http.post(url, data, this.httpOptions);
    return response;
  }
  addClient(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-client';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  }  
  addEmployee(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-employee';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  } 
  getEmployees(data){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-employees?saloon=' + data;
    

    const response = this.http.get(url, this.httpOptions);
    return response;
  }

  updateEmployee(params: string, data) {
    this.setLoadingStatus(true);
    const url = this.BASE_URL + 'update-employee/' + params;
  
    const response = this.http.patch(url, data, this.httpOptions);
  
    
  
    return response;
  }

  deleteEmployee(params: string) {
    this.setLoadingStatus(true);
  
  
    let url = this.BASE_URL + 'delete-employee/' + params;
  
    const response = this.http.delete(url, this.httpOptions);
  
    
  
    return response;
  }
  getServiceTotals(saloon, day){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'service-totals?saloon=' + saloon +'&day=' + day;
    

    const response = this.http.get(url, this.httpOptions);
    return response;
  }
  getExpenses(saloon, day){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-expenses?saloon=' + saloon +'&day=' + day;
    

    const response = this.http.get(url, this.httpOptions);
    return response;
  }

  addExpense(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-expense';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  } 
  editProfile(params: string, data) {
    this.setLoadingStatus(true);
    const url = this.BASE_URL + 'update-saloon/' + params;
  
    const response = this.http.patch(url, data, this.httpOptions);
  
    
  
    return response;
  }
  getUser(saloon){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-user?saloon=' + saloon;
    

    const response = this.http.get(url, this.httpOptions);
    return response;
  }

  getServices(saloon){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-services?saloon=' + saloon;
    

    const response = this.http.get(url, this.httpOptions);
    return response;
  }
  
  addService(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-service';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  } 
  editUser(params: string, data) {
    this.setLoadingStatus(true);
    const url = this.BASE_URL + 'update-admin/' + params;
  
    const response = this.http.patch(url, data, this.httpOptions);
  
    
  
    return response;
  }
  getPeriodicvisit(data){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'periodic-visits';
    

    const response = this.http.post(url, data, this.httpOptions);
    return response;
  }
  getEmployeeReport(data, params){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'employee-report/' + params;
    

    const response = this.http.post(url, data, this.httpOptions);
    return response;
  }

  superLogin(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'super-login';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  } 
  getPeriodicSaloonsReport(data){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'periodic-saloons-report';
    

    const response = this.http.post(url, data, this.httpOptions);
    return response;
  }
  updatePermission(saloon, permission){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'update-permission?saloon=' + saloon +'&permission=' + permission;
    

    const response = this.http.get(url, this.httpOptions);
    return response;
  }
  getSubscribers(data, day){
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'get-subscribers?saloon=' + data +'&day=' + day;;
    

    const response = this.http.get(url, this.httpOptions);
    return response;
  }

  addSubscriber(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'add-subscriber';
    

    const response = this.http.post(url, data, this.httpOptions);

    return response;
  } 
  editTimes(data) {
    this.setLoadingStatus(true);


     const url = this.BASE_URL + 'edit-times?id=' + data;
    

    const response = this.http.patch(url, data, this.httpOptions);

    return response;
  } 
}

